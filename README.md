# FlImage

A tool to flash disk images to block devices like SD cards, thumb drives or hard drives. The prime intention is to flash images for devices that run on SD card, but FlImage can also be used to flash installer images or backup disk images to available storage media.

## Get FlImage

Open a terminal and enter:

```
$ git clone https://gitlab.com/RandomErrorCode/flimage.git
```

This will download FlImage to your current work directory.

## Use FlImage

In a terminal, go into the project folder and run the tool:

```
$ cd flimage/
$ sudo ./flimage [IMAGE-FILE]
```

When a file is specified, you can flash it to the storage media you select.

SD cards are the preferred media type. If SD cards are detected in the internal card reader, you will be prompted to choose the correct card to be flashed. If no SD cards are found, you can choose between all other availabe media. SD cards inserted to a USB card reader are handled like regular flash drives.

To flash an image to a drive while having a SD card inserted in the internal card reader, use the '-d' or '--drive' option. It will let you choose one of the available drives to flash your image to.

## Additional command line options

**Drive mode**

Flash an image to a thumb drive or hard drive while having SD card inserted.

```
$ sudo ./flimage -d [IMAGE-FILE]
```

```
$ sudo ./flimage --drive [IMAGE-FILE]
```

**Display help**

Display a help text and get information about command options.

```
$ ./flimage -h
```

```
$ ./flimage --help
```

**Display version**

Display the version number, source repository and license.

```
$ ./flimage -v
```

```
$ ./flimage --version
```

## Example commands

You want to flash an OS image to SD card for your single-board computer.<br>
The image "distro.img" is in your home folder "~".

```
$ sudo ./flimage ~/distro.img
```

You want to flash a distro installer image to a USB thumb drive.<br>
The image "install.iso" is on your SD card, labeled "MySD".

```
$ sudo ./flimage -d media/$USER/MySD/install.iso
```

If there is no SD card inserted, the '-d' option can be omitted, as FlImage will then default to show all available storage media.

