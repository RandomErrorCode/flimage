## 1.0 (2021-07-22)

Initial release

**Implemented:**

- Basic functionality to flash disk images to block devices
- Prioritize SD cards over other storage media
- Select target from list of available or specified devices
- Confirmation prompt before flashing starts
- Permission check
- Proper handling of user input
- Basic error handling
- Handling of command line arguments
- Command line option for flashing to drives explicitly
- Command line options for displaying help and version
- README.md
- CHANGELOG.md
- LICENSE

